package ch.uzh.ifi.seal.changedistiller;

/*
 * #%L
 * ChangeDistiller
 * %%
 * Copyright (C) 2011 - 2017 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.File;
import java.util.List;

import ch.uzh.ifi.seal.changedistiller.ChangeDistiller;
import ch.uzh.ifi.seal.changedistiller.ChangeDistiller.Language;
import ch.uzh.ifi.seal.changedistiller.distilling.FileDistiller;
import ch.uzh.ifi.seal.changedistiller.model.entities.Delete;
import ch.uzh.ifi.seal.changedistiller.model.entities.Insert;
import ch.uzh.ifi.seal.changedistiller.model.entities.Move;
import ch.uzh.ifi.seal.changedistiller.model.entities.SourceCodeChange;
import ch.uzh.ifi.seal.changedistiller.model.entities.Update;

public class ChangeDistillerTester {

	public static void main(String[] args) {
		File left = new File("in/Test.java_v01");
		File right = new File("in/Test.java_v02");
		FileDistiller distiller = ChangeDistiller.createFileDistiller(Language.JAVA);
		try {
			distiller.extractClassifiedSourceCodeChanges(left, right);
		} catch(Exception e) {
			/* An exception most likely indicates a bug in ChangeDistiller. Please file a
		       bug report at https://bitbucket.org/sealuzh/tools-changedistiller/issues and
		       attach the full stack trace along with the two files that you tried to distill. */
			System.err.println("Warning: error while change distilling. " + e.getMessage());
		}
		List<SourceCodeChange> changes = distiller.getSourceCodeChanges();
		if(changes != null) {
			System.out.println("change is not null! " + changes.size());			
			for(SourceCodeChange change : changes) {
				if (change instanceof Delete) {
					System.out.println("Delete");
				}
				else if (change instanceof Insert) {
					System.out.println("Insert");
				}		
				else if (change instanceof Move) {
					System.out.println("Move");
				}		  
				else if (change instanceof Update) {
					System.out.println("Update");
				}		  
				// see Javadocs for more information
				System.out.println();
				System.out.println(change.getLabel());
				System.out.println(change.getRootEntity().getSourceCodeChanges());
				System.out.println(change.getParentEntity().getUniqueName());
				System.out.println(change.getChangedEntity().getSourceRange());
				System.out.println(change.getChangedEntity());				
				System.out.println(change.getChangedEntity().getUniqueName());
				System.out.println(change.getChangedEntity().getRelatedType());
				System.out.println(change.getRootEntity().getType()); // can distinguish class vs. methods by this!!!
				System.out.println(change.getRootEntity().getRelatedType()); // can distinguish class vs. methods by this!!!
				System.out.println(change.getRootEntity().getUniqueName());
				System.out.println(change.getRootEntity().getOldName());
				if (change instanceof Delete) {
				}
				else if (change instanceof Insert) {
				}		
				else if (change instanceof Move) {
				}		  
				else if (change instanceof Update) {
					System.out.println(((Update) change).getNewEntity().getUniqueName());
				}
			}
		}
		else {
			System.out.println("change is null");
		}
	}

}
